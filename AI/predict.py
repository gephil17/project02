# %%
import sys
import tensorflow as tf
import os

import numpy as np
from PIL import Image

model =  tf.keras.models.load_model('.')
model.summary()

img = Image.open('./pineapplebun.jpg')# , target_size=(32,32))

img = img.resize((180, 180))
img = np.array(img)
img = img / 255

img = img.reshape(-1, 180, 180, 3)
# logits = model(img, training=False)
# prediction = tf.argmax(logits, axis=1, output_type=tf.int32)
result = tf.argmax(model(img), axis=1, output_type=tf.int32)
print(result)

class_names = ['cocktail_bun', 'coconut_tart ', 'egg_tart', 'pineapple_bun', 'sausage_bun']

# img_class = imported.predict_classes(img) 
print("result: " + class_names[result.numpy()[0]])


# %%
