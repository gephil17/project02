import tornado.ioloop
import tornado.web
import tensorflow as tf
from tensorflow.keras.models import load_model
import numpy as np
from PIL import Image
import json

try:
    model = load_model('./trained-models/better-model', compile = False)
    print("Model Loaded.")
except Exception as e:
    print("ERROR:" + str(e))
    print("this is model error")

class MainHandler(tornado.web.RequestHandler):

    def post(self):
        content = json.loads(self.request.body)
        result = self.classify_bread_type(content['filename'])
        print(result)
        result = self.softmax(result)
        result = np.around(result, 3)
        self.write(str(result))

    def softmax(self, x):
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum(axis=0)

    def classify_bread_type(self, path):
        print(path)
        
        img = Image.open("../uploads/" + path)
        img = img.resize((180, 180))
        img = np.array(img)
        img = img / 255

        img = img.reshape((-1 ,) + img.shape)
        
        try:
            tem_predict = model.predict(img)

            print(f"this is tem_predict {tem_predict}")

            # result = tf.argmax(tem_predict, axis = 1, output_type=tf.int32)


            # result = tf.argmax(model.predict(img), axis=1, output_type=tf.int32)
        except Exception as e:
            print(e)

        # class_names = ['cocktail_bun', 'coconut_tart ', 'egg_tart', 'pineapple_bun', 'sausage_bun']

        # return class_names[result.numpy()[0]]
        return tem_predict[0]

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    PORT = 8888
    app.listen(PORT)
    print("http://localhost:" + str(PORT))
    tornado.ioloop.IOLoop.current().start()