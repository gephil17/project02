import * as bcrypt from 'bcryptjs';

const SALT_ROUNDS = 9;

export async function hashPassword(plainPassword:string) {
    const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
    return hash;
};

hashPassword('password').then(value => console.log(value));
hashPassword('Dragon').then(value => console.log(value));

hashPassword('Esther').then(value => console.log(value));

hashPassword('Jason_is_handsome').then(value => console.log(value));

