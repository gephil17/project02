{
   name: 'Sai Kung Café & Bakery',
   address: 'G/F, 6-7 Kam Po Court, 2 Hoi Pong Square, Sai Kung',
   address_for_map: '6-7 Kam Po Court, 2 Hoi Pong Square, Sai Kung',
   phone: '27923861',
   opening_hours: '0700-2130',
   cover_photos: "bakehouse.jpg"
}, {
   name: 'Say Hey Bakery',
   address: 'Shop B6-8, G/F, Por Mee Factory Building, 500 Castle Peak Road, Cheung Sha Wan',
   address_for_map: 'Por Mee Factory Building, 500 Castle Peak Road, Cheung Sha Wan',
   phone: '39570883',
   opening_hours: '0630-1900',
   cover_photos: "bakehouse.jpg"
}, {
   name: 'Danish Bakery',
   address: 'G/F, Leishun Court, 106 Leighton Road, Causeway Bay',
   address_for_map: 'Leishun Court, 106 Leighton Road, Causeway Bay',
   phone: '25767353',
   opening_hours: '0630-1900',
   cover_photos: "bakehouse.jpg"
}, {
   name: 'Happy Cake Shop',
   address: 'G/F, No.106 Queen's Road East, Wan Chai',
   address_for_map: 'No.106 Queen's Road East, Wan Chai',
   phone: '25281391',
   opening_hours: '0600-2000',
   cover_photos: "bakehouse.jpg"
}, {
   name: 'Ka Ka Chung Cake Shop',
   address: '11 Fu Wing Garden, Mong Kok',
   address_for_map: '11 Fu Wing Garden, Mong Kok',
   phone: '21758832',
   opening_hours: '0600-2200',
   cover_photos: "bakehouse.jpg"
}, {
   name: 'Door Door Bakery',
   address: 'G/F, 65 Broadway Street, Mei Foo Sun Chuen, Mei Foo',
   address_for_map: '65 Broadway Street, Mei Foo Sun Chuen, Mei Foo',
   phone: '23703880',
   opening_hours: '0600-2200',
   cover_photos: "bakehouse.jpg"
}, {
   name: 'Hoover Cake Shop',
   address: '136 Nga Tsin Wai Road, Kowloon City',
   address_for_map: '136 Nga Tsin Wai Road, Kowloon City',
   phone: '23820383',
   opening_hours: '0600-2200',
   cover_photos: "bakehouse.jpg"
}, {
   name: 'Tai Cheong Bakery',
   address: 'G/F, 35 Lyndhurst Terrace, Central',
   address_for_map: '35 Lyndhurst Terrace, Central',
   phone: '83008301',
   opening_hours: '0930-1930',
   cover_photos: "bakehouse.jpg"
}, {
   name: 'Louie Bakery',
   address: 'Shop G6, G/F, Dawning Views Plaza, Dawning Views, 23 Yat Ming Road, Fanling',
   address_for_map: 'Dawning Views Plaza, Dawning Views, 23 Yat Ming Road, Fanling',
   phone: '29555255',
   opening_hours: '0630-2045',
   cover_photos: "bakehouse.jpg"
}, {
   name: 'Baker Studio',
   address: 'Shop 2, G/F, Cheong Yiu Mansion, 51 Shiu Wo Street, Tsuen Wan,
   address_for_map: 'Cheong Yiu Mansion, 51 Shiu Wo Street, Tsuen Wan',
   phone: '26151523',
   opening_hours: '0630-2230',
   cover_photos: "bakehouse.jpg"
}, 