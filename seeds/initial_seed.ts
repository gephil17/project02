import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    await knex("photo").del();
    await knex('review_comment').del()
    await knex("review").del();
    await knex("bakery_comment").del();
    await knex("bakery").del();
    await knex("price_range").del();
    await knex("district").del();
    await knex("style").del();
    await knex("user").del();


    let userID = await knex('user').insert([
        /* you should always ALWAYS hash your password */
        {
            name: "Phil",
            // password: "$2a$09$lwR0xbsyS4WoRHOEmNFTPeIOUQVtq1rVlZYMCZuO/hmTDycplHyQG"
            password: "password",
            email: "email@email.com"
        }, {
            name: "Henry",
            // password: "$2a$09$lwR0xbsyS4WoRHOEmNFTPeIOUQVtq1rVlZYMCZuO/hmTDycplHyQG"
            password: "password",
            email: "henry@email.com"
        }, {
            name: "Esther",
            // password: "$2a$09$lwR0xbsyS4WoRHOEmNFTPeIOUQVtq1rVlZYMCZuO/hmTDycplHyQG"
            password: "password",
            email: "esther@email.com"
        }]).returning('id');
        
    // let styleID = await knex('style').insert({ name: "日式" }).returning('id');
    // let districtID = await knex('district').insert({ name: "荃灣" }).returning('id');
    // let priceRangeID = await knex('price_range').insert({ name: "$$$" }).returning('id');
    let bakeryID = await knex("bakery").insert([
         {
            name: 'Bakehouse',
            address: 'G/F, 14 Tai Wong Street East, Wan Chai',
            address_for_map: '14 Tai Wong Street East, Wan Chai',
            phone: '25667756',
            opening_hours: '0800-1800',
            cover_photos: "bakehouse.jpg"
        }, {
            name: 'Sweet Boutique de Tony Wong',
            address: `Shop B122A, B1/F, K11 Art Mall, 18 Hanoi Road, Tsim Sha Tsui`,
            address_for_map: `K11 Art Mall, 18 Hanoi Road, Tsim Sha Tsui`,
            phone: '27287768',
            opening_hours: '1130-2030',
            cover_photos: "sweetboutique.jpg"
        }, {
            name: 'Big Grains',
            address: `G/F, 61 Parkes Street, Jordan`,
            address_for_map: `61 Parkes Street, Jordan`,
            phone: '37020186',
            opening_hours: '0800-2030',
            cover_photos: "biggrains.jpg"
        }, {
            name: 'Hazukido',
            address: `Shop G11, G/F, AEON STYLE, Kornhill Plaza South, 1-2 Kornhill Road, Tai Koo`,
            address_for_map: `AEON STYLE, Kornhill Plaza South, 1-2 Kornhill Road, Tai Koo`,
            phone: '26010900',
            opening_hours: '1000-2045',
            cover_photos: "hazukido.jpg"
        }, {
            name: 'Lungo',
            address: `Shop B3B, G/F, Block B, Proficient Industrial Centre, 6 Wang Kwun Road, Kowloon Bay`,
            address_for_map: `Block B, Proficient Industrial Centre, 6 Wang Kwun Road, Kowloon Bay`,
            phone: '25813118',
            opening_hours: '0800-1800',
            cover_photos: "lungo.jpg"
        }, {
            name: 'Patisserie Uriwari',
            address: `Shop 409, 4/F, Life@KCC, 72-76 Kwai Cheong Road, Kwai Chung`,
            address_for_map: `Life@KCC, 72-76 Kwai Cheong Road, Kwai Chung`,
            phone: '21356705',
            opening_hours: '1100-2000',
            cover_photos: "patisserieuriwari.jpg"
        }, {
            name: 'LY Bakery',
            address: `Shop E, G/F, Maxgrand Plaza, 3 Tai Yau Street, San Po Kong`,
            address_for_map: `Maxgrand Plaza, 3 Tai Yau Street, San Po Kong`,
            phone: '35202122',
            opening_hours: '0700-2000',
            cover_photos: "lybakery.jpg"
        }, {
            name: 'Alisan Bakery',
            address: `Shop 5A, G/F, Hotel Sav, 83 Wuhu Street, Hung Hom`,
            address_for_map: `Hotel Sav, 83 Wuhu Street, Hung Hom`,
            phone: '23323118',
            opening_hours: '1200-2100',
            cover_photos: "alisan.jpg"
        }, {
            name: 'Queen Sophie',
            address: `G/F, 24 Hau Wong Road, Kowloon City`,
            address_for_map: `24 Hau Wong Road, Kowloon City`,
            phone: '23331728',
            opening_hours: '1000-2000',
            cover_photos: "queensophie.jpg"
        }, {
            name: 'Antoshimo Café & Bakery',
            address: `Shop G05B, G/F, Mikiki, 638 Prince Edward Road East, San Po Kong`,
            address_for_map: `Mikiki, 638 Prince Edward Road East, San Po Kong`,
            phone: '27110133',
            opening_hours: '1200-2200',
            cover_photos: "antoshimocafe.jpg"
        }, {
            name: 'ABC Cake House',
            address: `G/F, 255 Queen's Road East, Wan Chai`,
            address_for_map: `255 Queen's Road East, Wan Chai`,
            phone: '25440301',
            opening_hours: '0800-1900',
            cover_photos: "abccakehouse.jpg"
        }, {
            name: 'Bread Show',
            address: `G/F, Opulent Building, 396-416 Hennessy Road, Wan Chai`,
            address_for_map: `Opulent Building, 396-416 Hennessy Road, Wan Chai`,
            phone: '25491331',
            opening_hours: '0800-2000',
            cover_photos: "breadshow.jpeg"
        }, {
            name: 'Alive Food Co.',
            address: `G/F, 186 Tai Nan Street, Sham Shui Po`,
            address_for_map: `186 Tai Nan Street, Sham Shui Po`,
            phone: '91944219',
            opening_hours: '0900-1900',
            cover_photos: "alivefood.jpg"
        }, {
            name: 'Paper Stone Bakery',
            address: `Shop 1022, 1/F, YOHO MALL I, 9 Yuen Lung Street, Yuen Long`,
            address_for_map: `YOHO MALL I, 9 Yuen Lung Street, Yuen Long`,
            phone: '22531330',
            opening_hours: '0800-2130',
            cover_photos: "paperstone.jpg"
        }, {
            name: 'Levain Bakery',
            address: `Shop A, G/F, Tung Sing House, 138-144 Queen's Road East, Wan Chai`,
            address_for_map: `Tung Sing House, 138-144 Queen's Road East, Wan Chai`,
            phone: '27088112',
            opening_hours: '0800-1900',
            cover_photos: "levain.jpg"
        }, {
            name: 'Chez Choux',
            address: `Shop 2, Tsim Sha Tsui MTR Station, Tsim Sha Tsui`,
            address_for_map: `Tsim Sha Tsui MTR Station, Tsim Sha Tsui`,
            phone: '27303332',
            opening_hours: '0930-2300',
            cover_photos: "chezhouz.jpg"
        }, {
            name: 'Crostini Bakery & Café',
            address: `Shop G8, Kwun Tong Plaza, 68 Hoi Yuen Road, Kwun Tong`,
            address_for_map: `Kwun Tong Plaza, 68 Hoi Yuen Road, Kwun Tong`,
            phone: '23898613',
            opening_hours: '0800-2100',
            cover_photos: "crostini.jpg"
        }, {
            name: 'Maison Eric Kayser',
            address: `G/F, 248 Queen’s Road East, Wan Chai`,
            address_for_map: `248 Queen’s Road East, Wan Chai`,
            phone: '31071380',
            opening_hours: '0730-2030',
            cover_photos: "maisonerice.jpg"
        }, {
            name: 'La Creation & Chateraise',
            address: `Shop B224A, B/F, Times Square, 1 Matheson Street, Causeway Bay`,
            address_for_map: `Times Square, 1 Matheson Street, Causeway Bay`,
            phone: '25010032',
            opening_hours: '0730-2130',
            cover_photos: "lacreation.jpg"
        }, 
    ]).returning('id');
    await knex('bakery_comment').insert([
        {
            content: "testing1",
            user_id: userID[0],
            bakery_id: bakeryID[0]
        }, {
            content: "testing2",
            user_id: userID[1],
            bakery_id: bakeryID[0]
        }, {
            content: "testing3",
            user_id: userID[2],
            bakery_id: bakeryID[1]
        }
    ]);
    let reviewID = await knex('review').insert([
        {
            content: `test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1`,
            bakery_id: bakeryID[0],
            user_id: userID[0],
            uploads: 'farine-counter.png',
            title: '呢個係全英文的長篇review'
        }, {
            content: "我素來甚少主動要吃自助餐，直至看到網上關於The Market資料，心中竟然湧起無窮慾望要到來一試，在訂位後的短短一星期內，The Market更被多位食家熱捧起來。一次又一次，不停回味別人食評的相片，令我知道到來前一定要將整個胃空出來，可惜最後才發覺，一個胃原來是不可能足夠的...The Market位於尖東華懋廣場對面新落成不久的Hotel Icon二樓，由酒店設計裝修及員工制服，全部都是名家設計，我不是研究名牌專家，不打算作深入研究，但可以肯定這餐廳的環境光猛開揚，令食客用餐心情也會好一點。先參觀整間餐廳食物區，目的當然想讓相機先吃個飽。拍下一張又一張的照片，開始驚訝這個收費$218的自助午餐怎麼如此豐富。除了常見的生蠔外，差不多你想得到的種類都會有，無論冷盤沙律，魚生壽司，中西熱盤，粥粉麵飯，冷熱甜點全部都有，相機不知不覺先已吃了數十多張相片，它的主人亦要開始吃個痛快了！",
            bakery_id: bakeryID[0],
            user_id: userID[1],
            uploads: 'farine-counter.png',
            title: '呢個係全中文的長篇review'
        }, {
            content: "test review 3",
            bakery_id: bakeryID[1],
            user_id: userID[2],
            uploads: 'homemade-bread.jpg',
            title: '呢個係全英文的短篇review'
        }
    ]).returning('id');

    await knex('review_comment').insert([{
        user_id: userID[1],
        review_id: reviewID[0],
        content: 'Henry agger 第1個review'
    }, {
        user_id: userID[1],
        review_id: reviewID[1],
        content: 'Henry agger 第2個review'
    }, {
        user_id: userID[2],
        review_id: reviewID[1],
        content: 'Esther agger 第2個review'
    }])


    await knex('photo').insert([
        {
            path: "bread-3707013_960_720.jpg",
            review_id: reviewID[0],
        }, {
            path: "Lima-El-Pan-de-la-Chola-Breads-2.jpg",
            review_id: reviewID[0],
        }, {
            path: "sweet-breakfast.jpg",
            review_id: reviewID[1],
        }, {
            path: "bread-main.jpg",
            review_id: reviewID[2],
        }
    ])
};