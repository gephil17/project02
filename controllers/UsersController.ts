// call service
// validation
// handle error
// handle any related to request and response (session, redirection, json)



import type express from 'express'
import type { UsersService } from "../services/UsersService"

export class UsersController {
    public constructor(private usersService: UsersService) {

    }

    public checkLogin = (req: express.Request, res:express.Response) => {
        if (req.session['user']) {
            res.send("true");
        } else {
            res.send("false")
        }
    }

    // registration and login
    public createUser = async (req: express.Request, res: express.Response) => {
        let { username, password, email } = req.body;
        let result = await this.usersService.registerNewUser(username, password, email);
        if(!result){
            res.json({result})
        } else {
            let user = await this.usersService.getUserByEmail(email)
            req.session['user'] = user[0]
            res.json({result})
        }
    }

    public login = async (req: express.Request, res: express.Response) => {
        let { email, password } = req.body;
        let user = await this.usersService.getUserByEmail(email)
        if (user[0]?.password == password){
            req.session['user'] = user[0];
            // req.session['userid] = user[0] ??
            res.json({result: true})
        } else {
            res.json({result: false})
        }
    }

    public logout = (req: express.Request, res: express.Response) => {
        req.session['user'] = null;
        res.json({result: "Logged out."})
    }

    // // search bar
    // public searchUsers = async (req: express.Request, res: express.Response) => {

    // }
    public changePassword = async (req: express.Request, res: express.Response) => {
        let result = await this.usersService.updatePassword(req.session['user'].id, req.body['old-password'], req.body['new-password'])
        res.json({result})
    }

}