import type express from 'express';
import type { ProfileService } from "../services/ProfileService";

export class ProfileController {
    public constructor(private profileService: ProfileService) { }

    
    public getUserInfos = async (req: express.Request, res: express.Response) => {
        let userInfos = await this.profileService.selectUserInfos(req.session['user'].id);
        res.json(userInfos);
    }

    public getUserReviews = async (req: express.Request, res: express.Response) => {
        let userReviews = await this.profileService.selectUserReviews(req.session['user'].id);
        res.json(userReviews);
    }

    public getUserComments = async (req: express.Request, res: express.Response) => {
        let userComments = await this.profileService.selectUserComments(req.session['user'].id);
        res.json(userComments);
    }
} 