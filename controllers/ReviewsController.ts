// call service
// validation
// handle error
// handle any related to request and response (session, redirection, json)

import type express from 'express'
import type { ReviewsService } from "../services/ReviewsService";

export class ReviewsController {
    public constructor(private reviewsService: ReviewsService) {

    }

    public loadReviews = async (req: express.Request, res: express.Response) => {
        res.json(await this.reviewsService.loadReviewsFromDatabase())
    }

    public loadReview = async (req: express.Request, res: express.Response) => {
        res.json(await this.reviewsService.loadReviewFromDatabase(req.params.reviewID))
    }

    public createReview = async (req: express.Request, res: express.Response) => {
        // console.log(req.body)
        // console.log(req.session['user'])
        // console.log(req.files[0])

        // console.log(req.body.uploads)
        let files: any = req.files
        let review: any = await this.reviewsService.insertReview(req.session['user'].id, req.body.title, req.body.content, req.body.bakery_id)
        console.log(review)
        files.forEach(async (el: any) => await this.reviewsService.insertReviewPic(review[0], req.body.bakery_id, el.filename))
        

        // let reviewPic = await this.reviewsService.insertReviewPic(review.id, req.file.filename)
        // console.log(reviewPic)
        res.json({ result: "good" })
    }

    public commentReviews = async (req: express.Request, res: express.Response) => {
        console.log(req.session['user'])
        console.log(req.body)
        res.json(await this.reviewsService.insertReviewsComment(req.session['user'].id, req.body.content, req.body.review_id))
    }


}