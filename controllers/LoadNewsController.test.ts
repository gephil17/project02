// 如果要諗controller test case, 是咪應該由act開始諗,
// 例如await classInController.methodA(req, res),
// 先去controller到開個classInController, 裡面有methodA = async (req, res)
// 跟住視乎req/res, 睇下需唔需要俾野去service處理,
// 需要就再去service開class(要喺controller import返), 裡面有不同嘅async method
// …………………………………………
// 去返controller test case, 喺arrange到fake果啲req/res/method/method value,
// 最後喺assertion到睇下res俾嘅野啱唔啱, 行嘅過程中有冇用晒methodA裡面要用嘅service method
// …………………………………………
// 之後試下arrange裡面fake唔同嘅野, 用黎模擬唔同情況, 喺assert裡面expect唔同嘅結果


import { LoadNewsController } from "./LoadNewsController"

describe('Load News Controller', () => {
    let loadNewsController: LoadNewsController
    let req: any
    let res: any

    beforeEach(() => {
        loadNewsController = new LoadNewsController();
        req = jest.fn();
    });

    // load news test
    // arrangement is not finished, should add/del things depends on the test
    it('can return one piece of news', async () => {
        // Arrange
        res = {
            json: {
                "link": "testing link",
                "title": "testing title",
                "content": "testing content"
            },
            status: jest.fn().mockReturnThis()
        };

        // Act
        await loadNewsController.loadNews(req, res)

        // Assert
        // expect(getUserByUsername).toHaveBeenCalled();
        expect(res.json.link).toBe(
            "testing link"
        )
    })

    it('can return multiple pieces of news', async () => {
        // Arrange
        res = {
            json: [{
                "link": "1 testing link",
                "title": "1 testing title",
                "content": "1 testing content"
            }, {
                "link": "2 testing link",
                "title": "2 testing title",
                "content": "2 testing content"
            }],
            status: jest.fn().mockReturnThis()
        };

        // Act
        await loadNewsController.loadNews(req, res)

        // Assert
        expect(res.json[1].link).toBe(
            "2 testing link"
        )
        expect(res.json).toHaveLength(2)
    })

})
