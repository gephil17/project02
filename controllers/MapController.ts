// call service
// validation
// handle error
// handle any related to request and response (session, redirection, json)

import type express from 'express'
import type { MapService } from "../services/MapService";

export class MapController {
    public constructor(private mapService: MapService) {

    }

    public bakery = async (req: express.Request, res: express.Response) => {
        console.log("map controller")
        res.json(await this.mapService.getAllBakery())
    }
} 