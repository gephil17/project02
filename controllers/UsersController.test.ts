// 如果要諗controller test case, 是咪應該由act開始諗,
// 例如await classInController.methodA(req, res),
// 先去controller到開個classInController, 裡面有methodA = async (req, res)
// 跟住視乎req/res, 睇下需唔需要俾野去service處理,
// 需要就再去service開class(要喺controller import返), 裡面有不同嘅async method
// …………………………………………
// 去返controller test case, 喺arrange到fake果啲req/res/method/method value,
// 最後喺assertion到睇下res俾嘅野啱唔啱, 行嘅過程中有冇用晒methodA裡面要用嘅service method
// …………………………………………
// 之後試下arrange裡面fake唔同嘅野, 用黎模擬唔同情況, 喺assert裡面expect唔同嘅結果


import { UsersController } from "./UsersController"
import { UsersService } from "../services/UsersService"

describe('Users Controllers', () => {
    let usersController: UsersController
    let usersService: UsersService
    let res: any

    beforeEach(() => {
        usersController = new UsersController(usersService)
        usersService = new UsersService({} as any)
        // the {}  as any in the above line is for mocking a knex?
        res = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }
    });

    // registration test
    // arrangement is not finished, should add/del things depends on the test
    it('can create user', async () => {
        // Arrange
        const req: any = {
            body: {
                username: 'creating user',
                password: '123456',
                secondPassword: "123456",
                email: 'creating@gmail.com'
            }
        }

        // Act
        await usersController.createUser(req, res)
        
        // Assert
        // expect()

    })

    // it('cannot to create user due to registered email', async () => {
    //     // Arrange
    //     const req: any = {
    //         body: {
    //             username: 'creating user',
    //             password: '123456',
    //             secondPassword: "123456",
    //             email: 'creating@gmail.com'
    //         }
    //     }
    //     // Act
    //     await usersController.createUser(req, res)
    //     // Assert
    // });

    // it('cannot to create user due to failure to double-confirm password', async () => {
    //     // Arrange
    //     const req: any = {
    //         body: {
    //             username: 'creating user',
    //             password: '123456',
    //             secondPassword: "abcdef",
    //             email: 'creating@gmail.com'
    //         }
    //     }
    //     // Act
    //     await usersController.createUser(req, res)
    //     // Assert
    // });

    // it('cannot to create user due to failure to input correct email', async () => {
    //     // Arrange
    //     const req: any = {
    //         body: {
    //             username: 'creating user',
    //             password: '123456',
    //             secondPassword: "abcdef",
    //             email: 'email without @ and with space is a wrong email'
    //         }
    //     }
    //     // Act
    //     await usersController.createUser(req, res)
    //     // Assert
    // });

    // // login test
    // // arrangement is not finished, should add/del things depends on the test
    // it('can login', async () => {
    //     // Arrange
    //     const req: any = {
    //         body: {
    //             username: 'creating user',
    //             password: '123456',
    //         }
    //     }
    //     // Act
    //     await usersController.login(req, res)
    //     // Assert
    // })

    // it('cannot login due to wrong username', async () => {
    //     // Arrange
    //     const req: any = {
    //         body: {
    //             username: 'creating user',
    //             password: '123456',
    //         }
    //     }
    //     // Act
    //     await usersController.login(req, res)
    //     // Assert
    // })

    // it('cannot login due to wrong password', async () => {
    //     // Arrange
    //     const req: any = {
    //         body: {
    //             username: 'creating user',
    //             password: '123456',
    //         }
    //     }
    //     // Act
    //     await usersController.login(req, res)
    //     // Assert
    // })

    // // search bar test
    // it('can search existing users', async () => {
    //     // Arrange
    //     const req: any = {
    //         body: {
    //             username: 'creating user'
    //         }
    //     }
    //     // Act
    //     await usersController.searchUsers(req, res)
    //     // Assert
    // })

})