// 如果要諗controller test case, 是咪應該由act開始諗,
// 例如await classInController.methodA(req, res),
// 先去controller到開個classInController, 裡面有methodA = async (req, res)
// 跟住視乎req/res, 睇下需唔需要俾野去service處理,
// 需要就再去service開class(要喺controller import返), 裡面有不同嘅async method
// …………………………………………
// 去返controller test case, 喺arrange到fake果啲req/res/method/method value,
// 最後喺assertion到睇下res俾嘅野啱唔啱, 行嘅過程中有冇用晒methodA裡面要用嘅service method
// …………………………………………
// 之後試下arrange裡面fake唔同嘅野, 用黎模擬唔同情況, 喺assert裡面expect唔同嘅結果

import { ReviewsController } from "./ReviewsController"
import { ReviewsService } from "../services/ReviewsService"
import Knex from "knex"
const knexConfig = require('../knexfile')
const knex = Knex(knexConfig['testing'])


describe("reviews functions, including reviews in public page and in users' page", () => {
    let reviewsController: ReviewsController
    let reviewsService: ReviewsService
    let req: any
    let res: any

    beforeEach(() => {
        reviewsService = new ReviewsService(knex);
        reviewsController = new ReviewsController(reviewsService);
        res = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }
    })

    it(`can load ALL reviews from data base`, async () => {
        // Arrange
        const reviews = jest.spyOn(reviewsService, "loadReviewsFromDatabase")
        reviews.mockResolvedValue([{
            username: 'alice',
            bakery: '測試用麵包店一',
            photo: 'bread-main.jpg',
            content: '麵包好好食',
            comment_user: 'bob',
            comment_content: 'agger'
        }, {
            username: 'bob',
            bakery: '測試用麵包店2',
            photo: 'bread-main.jpg',
            content: 'hahahaha食',
            comment_user: '333',
            comment_content: 'agger'
        }])

        // Act
        await reviewsController.loadReviews(req, res)

        // Assert
        expect(reviews).toHaveBeenCalled();
        expect(res.json).toHaveBeenCalledWith([{
            username: 'alice',
            bakery: '測試用麵包店一',
            photo: 'bread-main.jpg',
            content: '麵包好好食',
            comment_user: 'bob',
            comment_content: 'agger'
        }, {
            username: 'bob',
            bakery: '測試用麵包店2',
            photo: 'bread-main.jpg',
            content: 'hahahaha食',
            comment_user: '333',
            comment_content: 'agger'
        }])
        // expect(res.json).toHaveLength(2)
    })

    it(`can load a specific review from data base`, async () => {
        // Arrange
        const reviews = jest.spyOn(reviewsService, "loadReviewFromDatabase")
        reviews.mockResolvedValue([{
            username: 'alice',
            bakery: '測試用麵包店一',
            photo: 'bread-main.jpg',
            content: '麵包好好食',
            comment_user: 'bob',
            comment_content: 'agger'
        }])

        // Act
        await reviewsController.loadReview(req, res)

        // Assert
        expect(reviews).toHaveBeenCalled();
        expect(res.json).toHaveBeenCalledWith([{
            username: 'alice',
            bakery: '測試用麵包店一',
            photo: 'bread-main.jpg',
            content: '麵包好好食',
            comment_user: 'bob',
            comment_content: 'agger'
        }])
    })


    it(`can comment on reviews`, async () => {
        // Arrange
        const comment = jest.spyOn(reviewsService, "insertReviewsComment")
        const req: any = {
            session: {
                'user': {
                    id: 1
                }
            },
            body: {
                content: "give comment to a review",
                review_id: 1
            }

        }

        // Act
        await reviewsController.commentReviews(req, res)

        // Assert
        expect(comment).toHaveBeenCalled();
    })

    it(`cannot comment reviews`, async () => {
        // Arrange
        jest.spyOn(reviewsService, "insertReviewsComment")
        const req: any = {
            session: {
                'user': {
                    id: 1
                }
            },
            body: {
                content: "give comment to a review",
                // review_id: 1
            }
        }

        // Act
        await reviewsController.commentReviews(req, res)

        // Assert
        expect(reviewsService.insertReviewsComment).toThrowError;

    })


    // it('can create a review', async () => {
    //     // Arrange
    //     const req: any = {
    //         body: {
    //             content: '試下create review',
    //         },
    //         file: {
    //             filename: 'test.png'
    //         },
    //         session: {}
    //     }
    //     const createReview = jest.spyOn(reviewsService, 'insertReviewIntoDataBase')
    //     createReview.mockResolvedValue({
    //         id: '1',
    //         file: 'test.png',
    //         content: '試下create review'
    //     })

    //     // Act
    //     await reviewsController.createReviews(req, res)

    //     // Assert
    //     expect(createReview).toHaveBeenCalled()
    //     expect(createReview).toHaveBeenCalledWith(
    //         '試下create review',
    //         'test.png',
    //         undefined

    //     );
    //     expect(res.json).toHaveBeenCalledWith({
    //         id: '1'
    //     })

    // })


})