import type express from 'express'
import type { BakeryService } from "../services/BakeryService"

export class BakeryController {
    public constructor(private bakeryService: BakeryService) {

    }

    public postComment = async (req: express.Request, res: express.Response) => {
        await this.bakeryService.insertComment(req.body.content, req.body.bakery_id, req.session['user'].id)
        res.json({result: "comment posted"})
    }

    public loadAllBakeries = async (req: express.Request, res: express.Response) => {
        let allBakeries = await this.bakeryService.selectAllBakeries();
        res.json(allBakeries);
    }

    public loadBakeryPhotos = async (req: express.Request, res: express.Response) => {
        let photos = await this.bakeryService.selectBakeryPhotos(req.params.id);
        res.json(photos);
    }

    public loadBakeryComments = async (req: express.Request, res: express.Response) => {
        let comments = await this.bakeryService.selectBakeryComments(req.params.id);
        res.json(comments);
    }

    public loadBakeryReviews = async (req: express.Request, res: express.Response) => {
        let reviews = await this.bakeryService.selectBakeryReviews(req.params.id);
        res.json(reviews);
    }

    public loadBakeryByID = async (req: express.Request, res: express.Response) => {
        let bakery = await this.bakeryService.getBakeryByID(req.params.id)
        res.json(bakery)
    }

    public likeBakery = async (req: express.Request, res: express.Response) => {
        let boolean = await this.bakeryService.alterLike(req.params.id, req.session['user'].id);
        res.json({result: boolean})
    }

    public checkLike = async (req: express.Request, res: express.Response) => {
        let boolean = await this.bakeryService.checkLike(req.params.id, req.session['user'].id);
        res.json({result: boolean})
    }

}