// call service
// validation
// handle error
// handle any related to request and response (session, redirection, json)

import type express from 'express'
import json from 'jsonfile'
import path from 'path'

export class LoadNewsController {
    public constructor() {

    }

    public loadNews = async (req: express.Request, res: express.Response) => {
        let readJson = await json.readFile(path.resolve("./public/news.json"), "utf8")
        res.json(readJson)
    }
}