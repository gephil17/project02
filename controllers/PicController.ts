import type express from 'express';
import fetch from 'node-fetch';
import type { PicService } from "../services/PicService";

export class PicController {
    public constructor(private picService: PicService) { }

    public postPic = async (req: express.Request, res: express.Response) => {
        let result = await fetch('http://localhost:8888', {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({filename: req.file.filename})
        })
        let text = await result.text()
        console.log(text);
        
        res.json({result: text})
    }

    public crap = async (req: express.Request, res: express.Response) => {
        console.log(this.picService);

    }
} 