// 如果要諗controller test case, 是咪應該由act開始諗,
// 例如await classInController.methodA(req, res),
// 先去controller到開個classInController, 裡面有methodA = async (req, res)
// 跟住視乎req/res, 睇下需唔需要俾野去service處理,
// 需要就再去service開class(要喺controller import返), 裡面有不同嘅async method
// …………………………………………
// 去返controller test case, 喺arrange到fake果啲req/res/method/method value,
// 最後喺assertion到睇下res俾嘅野啱唔啱, 行嘅過程中有冇用晒methodA裡面要用嘅service method
// …………………………………………
// 之後試下arrange裡面fake唔同嘅野, 用黎模擬唔同情況, 喺assert裡面expect唔同嘅結果


import { MapController } from "./MapController"
import { MapService } from "../services/MapService"
import Knex from "knex"
const knexConfig = require('../knexfile')
const knex = Knex(knexConfig['testing'])


describe('load bakery from database and show them on google map', () => {
    let mapController: MapController
    let mapService: MapService
    let req: any
    let res: any

    beforeEach(() => {
        mapService = new MapService(knex);
        mapController = new MapController(mapService);
        res = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }
    });

    // load news test
    it('can load one bakery', async () => {
        // Arrange
        const bakeryMap = jest.spyOn(mapService, "getAllBakery")
        bakeryMap.mockResolvedValue([{
            name: 'tecky bakery',
            address: 'one-mid town 27/F',
            phone: '12345678',
            opening_hours: '0900-2100',
        }])

        // Act
        await mapController.bakery(req, res)

        // Assert
        // expect(getUserByUsername).toHaveBeenCalled();
        expect(mapService.getAllBakery).toHaveBeenCalled();
        expect(res.json).toHaveBeenCalledWith([{
            name: 'tecky bakery',
            address: 'one-mid town 27/F',
            phone: '12345678',
            opening_hours: '0900-2100',
        }])
    })

    it('can load more than one bakery', async () => {
        // Arrange
        jest.spyOn(mapService, "getAllBakery").mockImplementation(() => Promise.resolve(
            [{
                name: 'tecky bakery',
                address: 'one-mid town 27/F',
                phone: '12345678',
                opening_hours: '0900-2100',
            }, {
                name: 'abcde',
                address: '荃灣地鐵站',
                phone: '12345678',
                opening_hours: '0900-2100',
            }]
        ))

        // Act
        await mapController.bakery(req, res)

        // Assert
        // expect(getUserByUsername).toHaveBeenCalled();
        expect(mapService.getAllBakery).toHaveBeenCalled();
        expect(res.json).toHaveBeenCalledWith([{
            name: 'tecky bakery',
            address: 'one-mid town 27/F',
            phone: '12345678',
            opening_hours: '0900-2100',
        }, {
            name: 'abcde',
            address: '荃灣地鐵站',
            phone: '12345678',
            opening_hours: '0900-2100',
        }])
    })


    it('can load database bakery', async () => {
        // Arrange

        // Act
        await mapController.bakery(req, res)

        // Assert
        expect(res.json).toHaveBeenCalledWith([{
            name: '測試用麵包店一',
            address: '荃灣西鐵站',
            phone: '12345678',
            opening_hours: '0900-2100',
        }, {
            name: '測試用麵包店二',
            address: '荃灣地鐵站',
            phone: '12345678',
            opening_hours: '0900-2100',
        }])
    })


})


