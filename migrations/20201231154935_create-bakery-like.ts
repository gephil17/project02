import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('user')){
        await knex.schema.alterTable('user', (table)=>{
            table.text('liked_bakery_ids');
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('user')){
        await knex.schema.alterTable('user', (table)=>{
            table.dropColumn('liked_bakery_ids');
        })
    }
}

