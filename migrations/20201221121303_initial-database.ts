import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('user', table => {
        table.increments();
        table.string('name');
        table.string('password');
        table.string('email');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('district', table => {
        table.increments();
        table.string('name');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('price_range', table => {
        table.increments();
        table.string('name');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('style', table => {
        table.increments();
        table.string('name');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('bakery', table => {
        table.increments();
        table.string('name');
        table.text('address');
        table.string('phone');
        table.string('opening_hours');
        table.integer('style_id');
        table.foreign('style_id').references('style.id');
        table.integer('district_id');
        table.foreign('district_id').references('district.id');
        table.integer('price_range_id');
        table.foreign('price_range_id').references('price_range.id');
        table.text('like_ids');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('bread_category', table => {
        table.increments();
        table.string('name');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('bread_served', table => {
        table.increments();
        table.integer('bakery_id');
        table.foreign('bakery_id').references('bakery.id');
        table.integer('bread_category_id');
        table.foreign('bread_category_id').references('bread_category.id');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('bakery_comment', table => {
        table.increments();
        table.text('content');
        table.integer('bakery_id');
        table.foreign('bakery_id').references('bakery.id');
        table.integer('user_id');
        table.foreign('user_id').references('user.id');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('review', table => {
        table.increments();
        table.text('content');
        // review唔一定有specific 嘅 bakery 0.0??
        table.integer('bakery_id');
        table.foreign('bakery_id').references('bakery.id');
        table.integer('user_id');
        table.foreign('user_id').references('user.id');
        table.text('like_ids');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('review_comment', table => {
        table.increments();
        table.text('content');
        table.integer('review_id');
        table.foreign('review_id').references('review.id');
        table.integer('user_id');
        table.foreign('user_id').references('user.id');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('photo', table => {
        table.increments();
        table.string('path');
        table.integer('review_id');
        table.foreign('review_id').references('review.id');
        table.integer('bakery_id');
        table.foreign('bakery_id').references('bakery.id');
        table.text('like_ids');
        table.timestamps(false, true);
    })

    await knex.schema.createTable('tag', table => {
        table.increments();
        table.string('content');
        table.text('post_ids');
        table.timestamps(false, true);
    })

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('tag');
    await knex.schema.dropTable('photo');
    await knex.schema.dropTable('review_comment');
    await knex.schema.dropTable('review');
    await knex.schema.dropTable('bakery_comment');
    await knex.schema.dropTable('bread_served');
    await knex.schema.dropTable('bread_category');
    await knex.schema.dropTable('bakery');
    await knex.schema.dropTable('style');
    await knex.schema.dropTable('price_range');
    await knex.schema.dropTable('district');
    await knex.schema.dropTable('user');

}

