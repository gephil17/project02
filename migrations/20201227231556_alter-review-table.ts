import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('review', table => {
        table.integer('photo_id').notNullable();
        table.foreign('photo_id').references('photo.id')
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.table('review', table => {
        table.dropColumn('photo_id')
    })
}

