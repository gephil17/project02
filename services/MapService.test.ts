import { MapService } from "./MapService"
import Knex from "knex"
const knexConfig = require('../knexfile')
const knex = Knex(knexConfig['testing'])

describe('map service', () => {
    let mapService: MapService;

    beforeEach(async () => {
        await knex.migrate.down();
        await knex.migrate.latest();
        await knex.seed.run();
        mapService = new MapService(knex)
    })

    it('can get bakery data', async () => {
        // arrange
        // act
        const bakery = await mapService.getAllBakery()
        // assert
        // expect(bakery["rows"]).toHaveLength(2)
        // expect(bakery["rows"][0].address).toEqual('荃灣西鐵站');
        expect(bakery).toHaveLength(2)
        expect(bakery[0].address).toEqual('荃灣西鐵站');
    })
})

afterAll(() => {
    knex.destroy()
})