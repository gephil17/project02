// talk with db
// talk with 3rd party (google, SMS, payment)
// handle business logic



import type Knex from "knex"


export class UsersService {
    public constructor(private knex: Knex) {

    }

    // registration and login
    public registerNewUser = async (username: string, password: string, email: string) => {
        let boolean = await this.checkExistingUserEmail(email);

        if (!boolean) {
            return (await this.knex
                .insert({
                    name: username,
                    password,
                    email
                })
                .into('user')
                .returning('id'))
        } else {
            return false;
        }
    }

    public checkExistingUserEmail = async (email: string) => {
        // should check if the email is already existed
        let existingEmails = await this.knex
            .select('*')
            .from('user')
            .where('email', email);

        if (existingEmails.length > 0) {
            return true;
        } else {
            return false
        }
    }

    // search bar
    public getUserByEmail = async (email: string) => {
        return (await this.knex
            .select('*')
            // .select('id') ??
            .from('user')
            .where('email', email));
    }

    public updatePassword = async (userID: string, oldPassword: string, newPassword: string) => {
        let password = await this.knex('user')
            .select('password')
            .where('id', userID)
            
            console.log(password[0].password);
            console.log(oldPassword);
            
        if(oldPassword == password[0].password){
            
            await this.knex('user')
                .where('id', userID)
                .update({'password': newPassword})
            return true
        }
        return false
    }

}