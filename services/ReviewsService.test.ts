import { ReviewsService } from "./ReviewsService"
import Knex from "knex"
const knexConfig = require('../knexfile')
const knex = Knex(knexConfig['testing'])

describe('review service', () => {
    let reviewsService: ReviewsService;

    beforeEach(async () => {
        await knex.migrate.rollback();
        await knex.migrate.latest();
        await knex.seed.run();
        reviewsService = new ReviewsService(knex)
    })

    // it('can get one piece of review', async () => {
    //     // arrange
    //     // act
    //     const reviews = await reviewsService.loadReviewsFromDatabase()
        
    //     // assert
    //     // expect(reviewsService.loadReviewsFromDatabase).toHaveBeenCalledWith()
    //     expect(reviews["reviews"][0]).toEqual({
    //         "content": "test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1", 
    //         "id": 1, 
    //         "created_at": "2021-01-05T14:33:09.067Z",
    //         "name": "Phil", 
    //         "uploads": "farine-counter.png",
    //         "path": "bread-3707013_960_720.jpg",
    //         "title": "呢個係全英文的長篇review",
    //     })

    // })

    it('can get more than one pieces of review', async () => {
        // arrange
        // act
        const reviews = await reviewsService.loadReviewsFromDatabase()
        
        // assert
        expect(reviews["reviews"]).toHaveLength(3)
        expect(reviews["reviewsComment"]).toHaveLength(3) 
        // expect(reviews["reviews"][0]).toEqual({"content": "麵包好好食", "id": 1, "name": "alice"})
        //expect(reviews[0].address).toEqual('荃灣西鐵站');
    })

    // it('can get a specific piece of review', async () => {
    //     // arrange
    //     // act
    //     const review = await reviewsService.loadReviewFromDatabase("1")
        
    //     // assert
    //     // expect(reviewsService.loadReviewsFromDatabase).toHaveBeenCalledWith()
    //     expect(review["reviews"][0]).toEqual({
    //         "content": "test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1 test review 1", 
    //         "id": 1, 
    //         "created_at": "2021-01-05T14:39:18.271Z",
    //         "name": "Phil", 
    //         "uploads": "farine-counter.png",
    //         "path": "bread-3707013_960_720.jpg",
    //         "title": "呢個係全英文的長篇review",
    //     })

    // })

    it('can comment a review', async () => {
        // arrange
        const req: any = {
            session: {
                'user': {
                    id: 1
                }
            },
            body: {
                content: "give comment to a review",
                review_id: 1
            }
        }

        // act
        const comment = await reviewsService.insertReviewsComment(req.session['user'].id, req.body.content, req.body.review_id)
        
        // assert
        expect(comment).toEqual({
            "content": ["give comment to a review"], 
            "name": [{"name": "Phil"}]
        })
        //expect(reviews["reviewsComment"]).toHaveLength(3) 
    })
})

afterAll(() => {
    knex.destroy()
})