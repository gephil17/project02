// talk with db
// talk with 3rd party (google, SMS, payment)
// handle business logic

import Knex from "knex"

export class ReviewsService {
    public constructor(private knex: Knex) {

    }

    public loadReviewsFromDatabase = async () => {
        let wholeReviews = {}
        const reviews = await this
            .knex('review')
            .join('user', 'review.user_id', 'user.id')
            .join('photo', 'review.id', 'photo.review_id')
            .select('user.name',
                // 'bakery.name',
                'review.id',
                'review.content',
                'review.title',
                'review.created_at',
                'photo.path')
            .distinctOn('photo.review_id')
            .orderBy('photo.review_id', 'desc')
            .orderBy('review.created_at', 'desc')

        const reviewsComment = await this
            .knex('review_comment')
            .leftJoin('user', 'review_comment.user_id', 'user.id')
            .leftJoin('review', 'review_comment.review_id', 'review.id')
            .select('user.name',
                'review_comment.review_id',
                'review_comment.content')
        wholeReviews = {
            reviews: reviews,
            reviewsComment: reviewsComment
        }
        return wholeReviews
    }

    public loadReviewFromDatabase = async (reviewID: string) => {
        let wholeReviews = {}
        const reviews = await this
            .knex('review')
            .join('user', 'review.user_id', 'user.id')
            .join('photo', 'review.id', 'photo.review_id')
            .select('user.name',
                'review.id',
                'review.content',
                'review.title',
                'review.created_at',
                'photo.path')
            .where('review.id', reviewID)
        

        const reviewsComment = await this
            .knex('review_comment')
            .leftJoin('user', 'review_comment.user_id', 'user.id')
            .leftJoin('review', 'review_comment.review_id', 'review.id')
            .select('user.name',
                'review_comment.review_id',
                'review_comment.content')
            .where('review.id', reviewID)
        wholeReviews = {
            reviews: reviews,
            reviewsComment: reviewsComment
        }
        return wholeReviews
    }

    public insertReview = async (userID: string, title: string, content: string, bakeryID: string) => {
        // const review = 
        return await this.knex.insert({
            user_id: userID,
            title,
            content,
            bakery_id: bakeryID
        }).into("review").returning('review.id')
    }

    public insertReviewPic = async (reviewID: string, bakeryID: string, file: string) => {
        return await this.knex.insert({
            review_id: reviewID,
            bakery_id: bakeryID,
            path: file
        }).into("photo")
    }

    public insertReviewsComment = async (user_id: number, content: string, review_id: number) => {
        let response = {}
        const commentContent = await this.knex.insert({ user_id, content, review_id })
            .into('review_comment')
            .returning('review_comment.content')
        const username = await this.knex('user')
                                    .select('name')
                                    .where('user.id', user_id)
                                    .returning('name')
        response = {
            name: username,
            content: commentContent,
        }
        return response
    }
}