// talk with db
// talk with 3rd party (google, SMS, payment)
// handle business logic

import Knex from "knex"

export class MapService {
    public constructor(private knex: Knex) {

    }

    public getAllBakery = async () => {
        const allBakery = await this.knex.select("name", "address", "phone", "opening_hours", "address_for_map").from("bakery")
        // const allBakery = await this.knex.raw("select name, address, phone, opening_hours from bakery")
        return allBakery
    }
}