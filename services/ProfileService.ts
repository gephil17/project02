import Knex from "knex"

export class ProfileService {
    public constructor(private knex: Knex) {}

    public selectUserInfos = async (id: string) => {
        return await this.knex('user').select('*')
            .where('id', id)
    }

    public selectUserReviews = async (id: string) => {
        return await this.knex('review').select('*')
            .where('user_id', id)
    }

    public selectUserComments = async (id: string) => {
        return await this.knex('bakery_comment').select('*')
            .where('user_id', id)
    }

}