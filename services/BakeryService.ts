import type Knex from "knex"

export class BakeryService {
    public constructor(private knex: Knex) {

    }

    public insertComment = async (content: string, bakery_id: number, user_id: number) => {
        return await this.knex.insert({ content, bakery_id, user_id })
            .into('bakery_comment')
            .returning('id')
    }

    public selectAllBakeries = async () => {
        return (await this.knex.select("*")
            .from("bakery")
            .orderBy('name'))
    }

    public selectBakeryPhotos = async (bakeryID: string) => {
        return (await this.knex.select('cover_photos')
            .from('bakery')
            .where('id', bakeryID))
    }

    public selectBakeryComments = async (bakeryID: string) => {
        return (await this.knex.select('*', 'user.name')
            .from('bakery_comment')
            .join('user', 'user.id', 'bakery_comment.user_id')
            .where('bakery_id', bakeryID))
    }

    public selectBakeryReviews = async (bakeryID: string) => {
        return (await this.knex.select('*', 'user.name', 'review.id')
            .from('review')
            .join('user', 'user.id', 'review.user_id')
            .where('bakery_id', bakeryID))
    }

    public getBakeryByID = async (bakeryID: string) => {
        return (await this.knex.select("*")
            .from('bakery')
            .where('id', bakeryID))
    }

    public alterLike = async (bakeryID: string, userID: string) => {
        let userIDs = [];
        let bakeryIDs = [];
        
        let res = (await this.knex('bakery')
            .select('like_ids')
            .where('id', bakeryID))[0];
        
        console.log(res);
        
        if(res.like_ids){
            userIDs = res.like_ids.split(",")
        }

        res = (await this.knex('user')
            .select('liked_bakery_ids')
            .where('id', userID))[0];

        if(res.liked_bakery_ids){
            bakeryIDs = res.liked_bakery_ids.split(",")
        }

        let userIDindex = userIDs.indexOf(userID.toString());
        console.log(userIDindex);
        let bakeryIDindex = bakeryIDs.indexOf(bakeryID.toString());
        if (userIDindex == -1) {
            userIDs.push(userID);
            bakeryIDs.push(bakeryID);
            
            await this.knex('bakery').where('id', bakeryID)
                .update({ 'like_ids': userIDs.join(',') });
            await this.knex('user').where('id', userID)
                .update({ 'liked_bakery_ids': bakeryIDs.join(',') });
            return true;
        } else {
            userIDs.splice(userIDindex, 1);
            bakeryIDs.splice(bakeryIDindex, 1);
            await this.knex('bakery').where('id', bakeryID)
                .update({ 'like_ids': userIDs.join(',') });
            await this.knex('user').where('id', userID)
                .update({ 'liked_bakery_ids': bakeryIDs.join(',') });
            return false;
        }
    }

    public checkLike = async (bakeryID: string, userID: string) => {
        let res = (await this.knex('user')
                            .select('liked_bakery_ids')
                            .where('id', userID))[0];

        let bakeryIDs: string[] = []
        if(res.liked_bakery_ids){
            bakeryIDs = res.liked_bakery_ids.split(",")
            if(bakeryIDs.filter(el => el == bakeryID).length > 0){
                return true;
            }
        }
        return false;
    } 
}