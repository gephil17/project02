import express from 'express';
// import { usersController } from './main';
import { LoadNewsController } from './controllers/LoadNewsController'
import { MapController } from './controllers/MapController'
import { MapService } from './services/MapService'
import { ReviewsController } from './controllers/ReviewsController'
import { ReviewsService } from './services/ReviewsService'

import Knex from 'knex';
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
// const knex = Knex(knexConfig[process.env.NODE_ENV || "testing"]);
import { usersController, bakeryController, picController, profileController } from './main';
import { isLoggedIn, upload } from './guards';

export const routes = express.Router();

// const uploads = multer({
//     dest: 'uploads/',
//     limits: {
//         fileSize: 10 * 1024 * 1024,
//     },
// });

routes.post('/signup', upload.any(), usersController.createUser)
routes.post('/login', upload.any(), usersController.login)
routes.get('/logout', usersController.logout)
routes.put('/password', upload.any(), usersController.changePassword)


// load news
const loadNewsController = new LoadNewsController
routes.get('/newsfeeds', loadNewsController.loadNews)

// load maps
const mapService = new MapService(knex)
const mapController = new MapController(mapService)
routes.get('/bakery-on-map', mapController.bakery)

// bakery related
routes.post('/bakery-comment', isLoggedIn, upload.any(), bakeryController.postComment)
routes.get('/get-bakery/:id', bakeryController.loadBakeryByID)
routes.get('/all-bakeries', bakeryController.loadAllBakeries)
routes.get('/bakery-photos/:id', bakeryController.loadBakeryPhotos)
routes.get('/bakery-comments/:id', bakeryController.loadBakeryComments)
routes.get('/bakery-reviews/:id', bakeryController.loadBakeryReviews)
routes.post('/like-bakery/:id', isLoggedIn, bakeryController.likeBakery)
routes.get('/like-bakery/:id', isLoggedIn, bakeryController.checkLike)

// reviews (reviews page)
const reviewsService = new ReviewsService(knex)
const reviewsController = new ReviewsController(reviewsService)
routes.get('/loadReviews', reviewsController.loadReviews)
routes.get('/loadReview/:reviewID', reviewsController.loadReview)
routes.post('/review-comment', isLoggedIn, upload.any(), reviewsController.commentReviews)
routes.post('/review', isLoggedIn, upload.array("uploads"), reviewsController.createReview)

// upload pics
routes.post('/bread-pic', upload.single('uploads'), picController.postPic)

// profile related
routes.get('/user-infos', isLoggedIn, profileController.getUserInfos)
routes.get('/user-reviews', isLoggedIn, profileController.getUserReviews)
routes.get('/user-comments', isLoggedIn, profileController.getUserComments)

/*
// high-level demo
// in your Route.ts
routes.get('/user/', isLoggedIn, demoRouter)

// in your UserRouter.ts
const demoRouter = express.Router();
demoRouter.get('info', profileController.getUserInfos)
demoRouter.get('review', profileController.getUserReviews)
demoRouter.get('comment', profileController.getUserComments)
*/
