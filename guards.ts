// import * as bcrypt from 'bcryptjs';
import { Request, Response, NextFunction } from 'express';
import multer from 'multer';

/* Dragon's comment - you should use the hashing*/
// const SALT_ROUNDS = 10;

// async function hashPassword(plainPassword:string) {
//     const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
//     return hash;
// };

// async function checkPassword(plainPassword:string,hashPassword:string){
//     const match = await bcrypt.compare(plainPassword,hashPassword);
//     return match;
// }

export function isLoggedIn(req:Request,res:Response,next:NextFunction){
    if(req.session?.['user']){
        next();
    }else{
        res.json({"result": "Not Logged In"});
    }
}



const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `${__dirname}/uploads`);
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
  })
export const upload = multer({storage})