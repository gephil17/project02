String.prototype.strToChars = function () {
    var chars = new Array();
    for (var i = 0; i < this.length; i++) {
        chars[i] = [this.substr(i, 1), this.isCHS(i)];
    }
    String.prototype.charsArray = chars;
    return chars;
}

String.prototype.isCHS = function (i) {
    if (this.charCodeAt(i) > 255 || this.charCodeAt(i) < 0) {
        return true;
    } else {
        return false;
    }
}

String.prototype.subCHString = function (start, end) {
    var len = 0;
    var str = "";
    this.strToChars();
    for (var i = 0; i < this.length; i++) {
        if (this.charsArray[i][1])
            len += 2;
        else
            len++;
        if (end < len)
            return str;
        else if (start < len)
            str += this.charsArray[i][0];
    }
    return str;
}

String.prototype.subCHStr = function (start, length) {
    return this.subCHString(start, start + length);
}



async function loadReviews() {
    //console.log('loading ALL reviews')
    let res = await fetch('/loadReviews')
    let json = await res.json()
    console.log(json)
    document.querySelector('#reviews-list').innerHTML = ``
    for (let i = 0; i < json["reviews"].length; i++) {
        // //this is for chinese
        if ((json["reviews"][i]['content']).isCHS(1) == true && json["reviews"][i]['content'].length >= 40) {
            let result = (json["reviews"][i]['content']).subCHStr(0, 80)
            //console.log(result)
            let a = result + `<span class='continue' onclick='redirectToReview(${json["reviews"][i]['id']})'> .....Continue reading</span>`
            //console.log(a)
            document.querySelector('#reviews-list').innerHTML += `
            <div class="review-container" data-id="${json["reviews"][i]['id']}">
                <div class="review-img">
                    <img src="${json["reviews"][i]['path']}" alt="Review">
                </div>

                <div class="review-title">
                    ${json["reviews"][i]['title']}
                </div>
                <div class="review-username">
                    ${json["reviews"][i]['name']}
                </div>
                <div class="review-created-time">
                    ${(json["reviews"][i]['created_at']).slice(0, 10)}
                </div>

                <div class="review-content">
                    ${a}
                </div>

                <div class="review-comment review-${json["reviews"][i]['id']}">
                    <div class="show-review-comment" onclick="showReviewComment()">
                        Hide comments
                    </div>
                    <div class="review-comment-container comment-shown">

                    </div>
                </div>

            </div>
            `
            for (let j = 0; j < json["reviewsComment"].length; j++) {
                if (json["reviewsComment"][j]["review_id"] == json["reviews"][i]["id"]) {
                    document.querySelector(`.review-${json["reviews"][i]['id']} > .review-comment-container`).innerHTML += `
                        <div class="each-review-comment">
                            <div class="review-comment-username">
                                ${json["reviewsComment"][j]["name"]}
                            </div>
                            <div class="review-comment-content">
                                ${json["reviewsComment"][j]["content"]}
                            </div>
                        </div>
                `
                }
            //commentReview()    
            }
            
        } else if ((json["reviews"][i]['content']).isCHS(0) == false && json["reviews"][i]['content'].length >= 20) {
            // this is for english
            let result = (json["reviews"][i]['content']).split(" ")
            let a = (result.slice(0, 20).join(' ')) + `<span class='continue' onclick='redirectToReview(${json["reviews"][i]['id']})'> .....Continue reading</span>`

            // console.log("check the first word")
            // console.log((json["reviews"][i]['content']).isCHS(0))
            document.querySelector('#reviews-list').innerHTML += `
            <div class="review-container" data-id="${json["reviews"][i]['id']}">
                <div class="review-img">
                    <img src="${json["reviews"][i]['path']}" alt="Review">
                </div>

                <div class="review-title">
                    ${json["reviews"][i]['title']}
                </div>
                <div class="review-username">
                    ${json["reviews"][i]['name']}
                </div>
                <div class="review-created-time">
                    ${(json["reviews"][i]['created_at']).slice(0, 10)}
                </div>

                <div class="review-content">
                    ${a}
                </div>

                <div class="review-comment review-${json["reviews"][i]['id']}">
                    <div class="show-review-comment" onclick="showReviewComment()">
                        Hide comments
                    </div>
                    <div class="review-comment-container comment-shown">

                    </div>
                </div>

            </div>
            `
            for (let j = 0; j < json["reviewsComment"].length; j++) {
                if (json["reviewsComment"][j]["review_id"] == json["reviews"][i]["id"]) {
                    document.querySelector(`.review-${json["reviews"][i]['id']} > .review-comment-container`).innerHTML += `
                        <div class="each-review-comment">
                            <div class="review-comment-username">
                                ${json["reviewsComment"][j]["name"]}
                            </div>
                            <div class="review-comment-content">
                                ${json["reviewsComment"][j]["content"]}
                            </div>
                        </div>

                `
                }
            //commentReview()    
            }
            
        } else {
            document.querySelector('#reviews-list').innerHTML += `
            <div class="review-container" data-id="${json["reviews"][i]['id']}">
                <div class="review-img">
                    <img src="${json["reviews"][i]['path']}" alt="Review">
                </div>

                <div class="review-title">
                    ${json["reviews"][i]['title']}
                </div>
                <div class="review-username">
                    ${json["reviews"][i]['name']}
                </div>
                <div class="review-created-time">
                    ${(json["reviews"][i]['created_at']).slice(0, 10)}
                </div>

                <div class="review-content">
                    ${json["reviews"][i]['content']}
                </div>

                <div class="review-comment review-${json["reviews"][i]['id']}">
                    <div class="show-review-comment" onclick="showReviewComment()">
                        Hide comments
                    </div>
                    <div class="review-comment-container comment-shown">

                    </div>
                </div>

            </div>
            `
            for (let j = 0; j < json["reviewsComment"].length; j++) {
                if (json["reviewsComment"][j]["review_id"] == json["reviews"][i]["id"]) {
                    document.querySelector(`.review-${json["reviews"][i]['id']} > .review-comment-container`).innerHTML += `
                        <div class="each-review-comment">
                            <div class="review-comment-username">
                                ${json["reviewsComment"][j]["name"]}
                            </div>
                            <div class="review-comment-content">
                                ${json["reviewsComment"][j]["content"]}
                            </div>
                        </div>
                `
                }
            //commentReview()    
            }
            
        }
    }
}



// add onclick on HTML

/* add onclick on HTML */

/* add onclick on HTML
 * it can do blahblahblah
 * the purpose is to blah blah
 */
function showReviewComment() {
    // const readButtons = document.querySelectorAll(".show-review-comment")
    console.log('click show comment button')
    if (event.currentTarget.parentElement.children[1].classList.contains('comment-shown')) {
        event.currentTarget.parentElement.children[1].classList.remove("comment-shown")
        event.currentTarget.parentElement.children[1].classList.add("comment-hidden")
        event.currentTarget.innerHTML = 'Show comments'

    } else if (event.currentTarget.parentElement.children[1].classList.contains('comment-hidden')) {
        event.currentTarget.parentElement.children[1].classList.remove("comment-hidden")
        event.currentTarget.parentElement.children[1].classList.add("comment-shown")
        event.currentTarget.innerHTML = 'Hide comments'
    }


}

function commentReview() {
    const submitButtons = document.querySelectorAll('.review-comment-text-area')
    // for (let submitButton of submitButtons) {

        submitButtons.forEach(el => 
            el.addEventListener('submit', async event => {
            event.preventDefault();
            let form = el;
            let formData = new FormData(form);
            let dataID = (event.target.parentElement).getAttribute('data-id')
            formData.append("review_id", (event.target.parentElement).getAttribute('data-id'))
            // formData.append("content", event.currentTarget.querySelector('.review-comment-text').innerHTML)
            // console.log(formData)
            let res = await fetch('/review-comment', {
                method: "POST",
                body: formData,
            })
            let result = await res.json();
            console.log(result)
            
            if (checkLoginStatus(result)) {
                form.querySelector('textarea').value = "";
            } 
            checkLoginStatus(result)
            let showReview = document.querySelector(`.review-${dataID} > .review-comment-container`)
            showReview.innerHTML += `
                <div class="each-review-comment">
                    <div class="review-comment-username">
                        ${result['name'][0].name}
                    </div>
                    <div class="review-comment-content">
                        ${result['content']}
                    </div>
                </div>
            `

            
        })
            
            )

    // }
}

function testingReview() {
    document.querySelector('.testbutton').addEventListener('click', (event) => {
        console.log('testing')
        console.log((event.currentTarget.parentElement).getAttribute('data-id'))
        console.log(event.target.parentElement.getAttribute('data-id'))

    })
}