async function redirectToReview(reviewID) {
    let res = await fetch(`/loadReview/${reviewID}`)
    let json = await res.json()
    console.log("redirect to a specific review")
    console.log(json);
    document.querySelector('#main').innerHTML += ``
    document.querySelector('#main').innerHTML = `
        <div class="specific-review-container" data-id="${json["reviews"][0]['id']}">
            <div class="img-back">
                <div class="back">
                    <i class="fas fa-arrow-left" onclick="toPage('reviews')"></i>
                </div>
                <div class="review-img">
                    <img src="${json["reviews"][0]['path']}" alt="Review">
                </div>
            </div>

            <div class="review-title">
                ${json["reviews"][0]['title']}
            </div>
            <div class="review-username">
                ${json["reviews"][0]['name']}
            </div>
            <div class="review-created-time">
                ${(json["reviews"][0]['created_at']).slice(0, 10)}
            </div>

            <div class="review-content">
                ${json["reviews"][0]['content']}
            </div>

            <div class="review-comment review-${json["reviews"][0]['id']}">
                <div class="show-review-comment" onclick="showReviewComment()">
                    Hide comments
                </div>
                <div class="review-comment-container comment-shown">

                </div>
            </div>
            <form action="/review-comment" class="review-comment-text-area">
                <textarea class="review-comment-text" name="content" placeholder="Comment here" required></textarea>
                <div name="review_id" value="${json["reviews"][0]['id']}"></div>
                <p></p>
                <input type="submit" value="Post" class="post-review-comment">
            </form>
        </div>
        `
    for (let j = 0; j < json["reviewsComment"].length; j++) {
        if (json["reviewsComment"][j]["review_id"] == json["reviews"][0]["id"]) {
            document.querySelector(`.review-${json["reviews"][0]['id']} > .review-comment-container`).innerHTML += `
                    <div class="each-review-comment">
                        <div class="review-comment-username">
                            ${json["reviewsComment"][j]["name"]}
                        </div>
                        <div class="review-comment-content">
                            ${json["reviewsComment"][j]["content"]}
                        </div>
                    </div>

            `
        }
    }
    commentReview()

}