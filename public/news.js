
async function showNews() {
    console.log('loading news')
    let res = await fetch('/newsfeeds')
    let json = await res.json()
    console.log(json)

    for (let i = 0; i < json.length; i++) {
        document.querySelector(".news-container").innerHTML += `
        <div class="news">
        <a href="${json[i].link}" target="_blank">
            <div class="news-title">
                ${json[i].title}
            </div>
            <div class="news-link">
                ${decodeURIComponent(json[i].link)}
            </div>
            <div class="news-abstract">
                ${json[i].content}
            </div>
        </a>
        </div>
        `
    }
}
