async function logout() {
    let res = await fetch('/logout')
    let json = await res.json()
    if (json.result == "Logged out.") {
        console.log("logout success");
        setTimeout(function () {
            toPage('home')
        }, 1500)
    }
}

async function loadProfileContents() {

    let infosRes = await fetch('/user-infos');
    let infos = await infosRes.json();
    console.log(infos);

    let reviewsRes = await fetch('/user-reviews');
    let reviews = await reviewsRes.json();
    console.log(reviews);

    let commentsRes = await fetch('/user-comments');
    let comments = await commentsRes.json();
    console.log(comments);

    let likesDIV = document.querySelector('#user-likes');
    let commentsDIV = document.querySelector('#user-comments');
    let reviewsDIV = document.querySelector('#user-reviews');

    document.querySelector('#user-name').innerHTML = infos[0].name;
    document.querySelector('#user-email').innerHTML = infos[0].email;

    if (infos[0]['liked_bakery_ids']) {
        for (let bakeryID of infos[0]['liked_bakery_ids'].split(',')) {
            let bakeryRes = await fetch(`get-bakery/${bakeryID}`)
            let bakery = await bakeryRes.json();

            likesDIV.innerHTML += `
            <div onclick="getBakery(${bakeryID})">
                <img src='./bakery_photos/high-class/${bakery[0]['cover_photos']}'>
                <span>${bakery[0]['name']}</span>
            </div>
            `
        }
    }

    if(reviews) {
        for (let review of reviews) {
            let bakeryRes = await fetch(`get-bakery/${review.bakery_id}`)
            let bakery = await bakeryRes.json()

            let contentRes = await fetch(`loadReview/${review.id}`);
            let content = await contentRes.json();

            reviewsDIV.innerHTML += `
            <div onclick="redirectToReview(${review.id})">
                <img src='./bakery_photos/high-class/${bakery[0]['cover_photos']}'>
                <span class="bakery-name">${bakery[0].name}</span>
                <b>${content.reviews[0].title}</b>
            </div>
            `
        }
    }

    if(comments){
        for (let comment of comments) {
            let bakeryRes = await fetch(`get-bakery/${comment.bakery_id}`);
            let bakery = await bakeryRes.json();
    
            commentsDIV.innerHTML += `
            <div onclick="getBakery(${bakery[0].id})">
                <img src='./bakery_photos/high-class/${bakery[0]['cover_photos']}'>
                <span class="bakery-name">${bakery[0].name}</span>
                <span class="comment">${comment.content}</span>
            </div>
            `
        }
    }

    loadChangePasswordForm()
}

function loadChangePasswordForm() {
    let form = document.querySelector('#change-password')
    form.addEventListener('submit', async (event) => {
        event.preventDefault();

        if (form['new-password'].value != form['new-password-check'].value) {
            form.querySelector('label > .alert').innerHTML = "New passwords do not match."
            return
        }

        let formData = new FormData(form);
        let res = await fetch('/password', {
            method: "PUT",
            body: formData
        })
        let json = await res.json()

        if (json.result == false) {
            form.querySelector('label > .alert').innerHTML = 'Old password is wrong.'
            return
        }

        form.querySelector('label > .alert').innerHTML = 'Password changed.'
    })
}