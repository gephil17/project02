function selectUserForms(hoverOrProfilePage) {
    let hoverDiv = document.querySelector('.hover');
    document.querySelector('#sign-up').addEventListener('submit', async (event) => {
        event.preventDefault();
        console.log('submit');
        let form = document.querySelector('#sign-up');
        if(form['password'].value != form['check-password'].value){
            document.querySelector('.sign-up.alert').innerHTML = "Passwords do not match."
            return
        }

        let formData = new FormData(form);
        let res = await fetch('/signup', {
            method: "POST",
            body: formData
        })
        let result = await res.json();
        console.log(result);
        if(!result.result){
            document.querySelector('.sign-up.alert').innerHTML = "Email already registered."
            return
        }

        document.querySelector('.sign-up.alert').innerHTML = "Success!"
        if(hoverOrProfilePage == "hover"){
            setTimeout(function(){
                hoverDiv.classList.add('hidden');
            }, 1500)
        } else {
            setTimeout(function(){
                toPage('profile')
            }, 1500)
        }
    })


    document.querySelector('#login').addEventListener('submit', async (event) => {
        event.preventDefault();
        let form = document.querySelector('#login');
        let formData = new FormData(form);
        let res = await fetch('/login', {
            method: "POST",
            body: formData
        })
        let result = await res.json();
        if(result.result == false){
            document.querySelector('.login.alert').innerHTML = "Login failed, please try again."
            return;
        }

        document.querySelector('.login.alert').innerHTML = "Success!"

        if(hoverOrProfilePage == "hover"){
            setTimeout(function(){
                hoverDiv.classList.add('hidden');
            }, 1500)
        } else {
            setTimeout(function(){
                toPage('profile')
            }, 1500)
        }
    });
}


async function checkLoginStatus(result) {
    if (result.result == "Not Logged In") {
        let res = await fetch('/login');
        let html = await res.text();
        let hoverDiv = document.querySelector('.hover');
        hoverDiv.innerHTML = html;
        hoverDiv.classList.remove('hidden');
        document.querySelector('#login-div').style.width = "max-content"
        document.querySelector('#login-div').style.backgroundColor = "white";
        selectUserForms("hover");
    }
}