async function loadAllBakeries(){
    let allBakeries;
    if(preloadBakeries == null){
        let res = await fetch('/all-bakeries')
        allBakeries = await res.json();
        console.log(allBakeries);
        preloadBakeries = allBakeries;
    } else {
        console.log("preload");
        allBakeries = preloadBakeries;
    }

    let bakeriesContainer = document.querySelector('#bakeries-container')
    bakeriesContainer.innerHTML = "";
    for(let bakery of allBakeries){
    
        let photoRes = await fetch(`/bakery-photos/${bakery.id}`);
        let photos = await photoRes.json();
        
        if(bakery.cover_photos){
            bakeriesContainer.innerHTML += `
            <div class="each-result demo-each-result" data-id="${bakery.id}">
                <div class="each-img">
                    <img onclick="getBakery(${bakery.id})" class="demo-pic-resize-1" src="./bakery_photos/high-class/${bakery.cover_photos}" alt="Bakery Information">
                </div>
                <div class="each-result-info" data-id="${bakery.id}">
                    <div>
                        <div class="each-info each-name">
                            <span onclick="getBakery(${bakery.id})" class="demo-span-ref">${bakery.name}</span>
                        </div>
                        <div class="demo-address-box">
                            <span onclick="getBakery(${bakery.id})" class="demo-span-ref">${bakery.address}</span>
                        </div>
                    </div>
                </div>
            </div>
            `
        } else {
            bakeriesContainer.innerHTML += `
            <div class="each-result-info" data-id="${bakery.id}">
    
                <div>
                    <div class="each-info each-name">
                        ${bakery.name}
                    </div>
                    <div onclick="getBakery(${bakery.id})" class="demo-address-box">
                        ${bakery.address}
                    </div>
                </div>
            </div>
            `
        }

    }
}

// <div class="each-result" onclick="getBakery(${bakery.id})" data-id="${bakery.id}">
// <div onclick="getBakery(${bakery.id})" class="each-img">
//     <img src="${photos[0].path}" alt="Bakery Information">
// </div>
// <div>
//     <div class="each-info each-name">
//         ${bakery.name}
//     </div>
//     <div class="each-info each-content">
//         ${bakery.address}
//     </div>
// </div>
// </div>

async function search(){
    let input = document.querySelector('#search').value.toLowerCase();

    let bakeries = preloadBakeries.filter(el => el['address'].toLowerCase().includes(input) || el['name'].toLowerCase().includes(input))
    
    let bakeriesContainer = document.querySelector('#bakeries-container')
    bakeriesContainer.innerHTML = "";
    for(let bakery of bakeries){
    
        let photoRes = await fetch(`/bakery-photos/${bakery.id}`);
        let photos = await photoRes.json();
        
        if(bakery.cover_photos){
            bakeriesContainer.innerHTML += `
            <div class="each-result" data-id="${bakery.id}">
                <div class="each-img">
                    <img onclick="getBakery(${bakery.id})" src="./bakery_photos/high-class/${bakery.cover_photos}" alt="Bakery Information">
                </div>
                <div class="each-result-info" data-id="${bakery.id}">
                    <div>
                        <div class="each-info each-name">
                            ${bakery.name}
                        </div>
                        <div onclick="getBakery(${bakery.id})" class="demo-address-box">
                            ${bakery.address}
                        </div>
                    </div>
                </div>
            </div>
            `
        } else {
            bakeriesContainer.innerHTML += `
            <div class="each-result-info" data-id="${bakery.id}">
    
                <div>
                    <div class="each-info each-name">
                        ${bakery.name}
                    </div>
                    <div onclick="getBakery(${bakery.id})" class="demo-address-box">
                        ${bakery.address}
                    </div>
                </div>
            </div>
            `
        }

    }
}

// redirect is implemented by onclick="getBakery(${bakery.id})"



