function selectPicUploader() {
    let form = document.querySelector("#bread-pic")
    form.addEventListener('submit', async (event) => {
        event.preventDefault();

        let formData = new FormData(form);
        let res = await fetch('/bread-pic', {
            method: "POST",
            body: formData,
        })

        let json = await res.json()

        // let fakeResult = "[0. 0. 0.002 0.998 0.]";
        let result = json.result.slice(1, -1).split(' ');
        result = result.filter(el => el != "")
        console.log(result);
        const divIDs = ["ctb", "ct", "et", "pb", "chd"];

        for (let index in divIDs){
            if (document.querySelector(`#${divIDs[index]} > span`)){
                document.querySelector(`#${divIDs[index]} > span`).remove()
            }
            document.querySelector(`#${divIDs[index]}`).innerHTML += `
                <span style="color: red;">${result[index] * 100}%</span>
            `
        }

    })
}

function printPic(event) {
    let input = event.target;

    let reader = new FileReader();
    reader.onload = function () {
        let dataURL = reader.result;
        let output = document.querySelector('#bread-pic-upload');
        output.src = dataURL;
    };
    reader.readAsDataURL(input.files[0]);
};
