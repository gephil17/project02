const displayDiv = document.querySelector(".function-display")

// async function loadBakeryList() {
//     let res = await fetch('XXX')
//     let list = await res.json()
// }

document.querySelector(".bakery-list").addEventListener("click", () => {
    displayDiv.innerHTML = ""
    console.log("load bakery list")
    // load Bakery list function should be used here
    displayDiv.innerHTML += `        <div class="each-result">
    <div class="each-img">
        <img src="plain_sandwich.jpg" alt="Bakery Information">
    </div>
    <div>
        <div class="each-info each-name">
            NAME
        </div>
        <div class="each-info each-content">
            ADDRESS, ADDRESS, ADDRESS, ADDRESS, ADDRESS
        </div>
    </div>
</div>`
})


// async function loadReviews() {
//     let res = await fetch('XXX')
//     let list = await res.json()
// }

document.querySelector(".reviews").addEventListener("click", () => {
    displayDiv.innerHTML = ""
    console.log("load reviews")
    // load reviews function should be used here
    displayDiv.innerHTML += `<div class="each-result">
    <div class="each-img">
        <img src="bread-main.jpg" alt="Review">
    </div>
    <div>
        <div class="each-info each-name">
            Username
        </div>
        <div class="each-info each-content">
            content content content content content content
        </div>
    </div>
</div>`
})
