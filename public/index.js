async function toPage(element) {
    if (element == 'profile') {
        let textRes = await fetch('/login-status');
        let text = await textRes.text();
        console.log(text);
        if (text != "true") {
            console.log("false");
            element = 'login'
        }
    }

    console.log(element);
    let res = await fetch(element);
    let html = await res.text();
    document.querySelector('article#main').innerHTML = html;

    if (element == 'login') {
        selectUserForms("login");
    } else if (element == 'news') {
        showNews()
    } else if (element == 'map') {
        googleMapWithPin()
    } else if (element == 'bakery-list') {
        loadAllBakeries();
    } else if (element == 'reviews') {
        loadReviews()
        commentReview()
        // showReviewComment() 
    } else if (element == 'bread-types') {
        selectPicUploader()
    } else if (element == 'profile') {
        loadProfileContents()
    }
}

toPage('home');

function showHideSubMenu() {
    const subMenu = document.querySelector(".sub-menu");
    if (subMenu.classList.contains("hidden")) {
        subMenu.classList.remove("hidden");
    } else {
        subMenu.classList.add("hidden")
    }
}


// const menuButtons = document.querySelectorAll('.sub-menu-btn')
// for (let menuButton of menuButtons) {
//     menuButton.addEventListener("click", function() {
//         if (menuButton.innerHTML == "Map") {
//             window.location.href = "./map.html"
//         }
//     })
// }



document.querySelector('.hover').addEventListener('click', (event) => {
    if (event.target.parentElement == document.querySelector('body')) {
        document.querySelector('.hover').classList.add('hidden');
    }
})

