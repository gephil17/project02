async function bakeryListOnGoogleMap() {
    // console.log('loading bakery')
    let res = await fetch('/bakery-on-map')
    let json = await res.json()
    // console.log(json)
    return json
}



function initMap() {
}

async function googleMapWithPin() {
    let map;
    let infoWindow;
    let latitudes = []
    let longitudes = []

    // infoWindow to collect all the info
    let infoWindows = [];
    let info_config = [];
    let markers = [];

    let promiseList = []
    let databaseBakery = await bakeryListOnGoogleMap();
    let newDiv = document.createElement('div');
    newDiv.id = 'map';
    document.querySelector('article#main').appendChild(newDiv);
    let hkMap = document.querySelector("#map")
    map = new google.maps.Map(hkMap, {
        center: { lat: 22.317001, lng: 114.169934 },
        zoom: 11,
    });

    // following Phil's method
    let currentInfoWindow = null;



    for (let i = 0; i < databaseBakery.length; i++) {
        let location = databaseBakery[i]["address_for_map"]

        let bakery = {
            position: {
                lat: 0,
                lng: 0
            },
            map: map,
            content: databaseBakery[i],
            icon: './hiclipartcom.png'
            
        }

        info_config.push(databaseBakery[i])

        let promise1 = new Promise((resolve, reject) => {
            axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: location,
                    key: 'AIzaSyB4L9BXrB0RH_4gQCGGVnSgVmG7f5l1Q_g'
                }
            })
                .then(function (response) {
                    if (response.data.status == "OK") {
                        bakery.position.lat = response.data.results[0].geometry.location.lat;
                        bakery.position.lng = response.data.results[0].geometry.location.lng;
                    }
                    resolve(true)
                })
        })
        // promiseList.push(promise1)
        await promise1;

        
        let marker = new google.maps.Marker(bakery)

        let infoWindow = new google.maps.InfoWindow({
            content: `<span style="font-weight: bold">${databaseBakery[i].name}</span>` +
                '<br>' + ` <span>地址: ${databaseBakery[i].address}</span>` +
                '<br>' + `<span>電話: ${databaseBakery[i].phone}</span>` +
                '<br>' + `<span>營業時間: ${databaseBakery[i].opening_hours}</span>`
        });

        google.maps.event.addListener(marker, 'click', function () {
            console.log(i)
            console.log(marker)
            console.log(infoWindow)
            
            // simple, but ineffective way
            // left as exercise to improve
            // infoWindows.forEach(singleInfoWindows => singleInfoWindows.close())

            // Phil's method
            if (currentInfoWindow != null) {
                console.log(currentInfoWindow)
                currentInfoWindow.close()
            }

            // infoWindow.close();






            infoWindow.open(map, marker);
            console.log(marker.content.name + ": " + marker.position);
            currentInfoWindow = infoWindow

        });

        infoWindows.push(infoWindow)
    }

    console.log(infoWindows)


}



/* Dragon's comment - delete all the commented, unnecessary code */

// async function googleMapWithPin() {
//     let map;
//     let infoWindow;
//     let latitudes = []
//     let longitudes = []

//     let infoWindows = [];
//     let info_config = [];
//     let markers = [];

//     let promiseList = []
//     let databaseBakery = await bakeryListOnGoogleMap();
//     let newDiv = document.createElement('div');
//     newDiv.id = 'map';
//     document.querySelector('article#main').appendChild(newDiv);



//     for (let i = 0; i < databaseBakery.length; i++) {
//         let location = databaseBakery[i]["address_for_map"]
//         // console.log(i + ": " + allLocations);

//         // console.log(databaseBakery[i])
//         // console.log("map address: " + allLocations)

//         info_config.push(databaseBakery[i])

//         // let locations = allLocations.split('\n')
//         // console.log(i + ": " + locations);
//         // for (let location of locations) {
//             await new Promise((resolve) => {
//                 setTimeout(resolve, 0.001)
//             })

//             let promise1 = new Promise((resolve, reject) => {
//                 axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
//                     params: {
//                         address: location,
//                         key: 'AIzaSyB4L9BXrB0RH_4gQCGGVnSgVmG7f5l1Q_g'
//                     }
//                 })
//                     .then(function (response) {
//                         if (response.data.status == "OK") {

//                             let latitude = response.data.results[0].geometry.location.lat;
//                             let longitude = response.data.results[0].geometry.location.lng;

//                             latitudes.push(latitude)
//                             longitudes.push(longitude)

//                         }
//                         resolve(true)
//                     })
//             })
//             promiseList.push(promise1)
//         // }
//         // console.log(promiseList);
//     }

//     //設定資訊視窗內容

//     // console.log(databaseBakery)
//     // console.log(latitudes)
//     // console.log(longitudes)

//     await Promise.all(promiseList).then((results) => {
//         console.log(results);
//         let hkMap = document.querySelector("#map")
//         map = new google.maps.Map(hkMap, {
//             center: { lat: 22.317001, lng: 114.169934 },
//             zoom: 11,
//         });

//         console.log("before 205")
//         for (let i = 0; i < latitudes.length; i++) {
//             // console.log(latitudes.length)
//             let marker = new google.maps.Marker({
//                 position: { lat: latitudes[i], lng: longitudes[i] },
//                 map: map,
//                 content: databaseBakery[i]
//             })
//             // console.log(i)
//             // console.log(latitudes[i])
//             // console.log(longitudes[i])
//             // console.log("===")

//             // console.log(longitudes[i])
//             // console.log(databaseBakery[i])

//             let infoWindow = new google.maps.InfoWindow({
//                 content: `<span style="font-weight: bold">${databaseBakery[i].name}</span>` +
//                     '<br>' + ` <span>地址: ${databaseBakery[i].address}</span>` +
//                     '<br>' + `<span>電話: ${databaseBakery[i].phone}</span>` +
//                     '<br>' + `<span>營業時間: ${databaseBakery[i].opening_hours}</span>`
//             });

//             google.maps.event.addListener(marker, 'click', function () {
//                 // console.log(i)
//                 // console.log(marker)
//                 infoWindow.open(map, marker);
//                 console.log(marker.content.name + ": " + marker.position);
//             });
//         }
//         console.log("after 205")
//     })
// }

