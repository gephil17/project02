function selectCommentForm() {
    document.querySelector('#bakery-comment').addEventListener('submit', async event => {
        event.preventDefault();
        let id = document.querySelector('.each-name').getAttribute("data-id");
        let form = document.querySelector('#bakery-comment');
        console.log(form)
        let formData = new FormData(form);
        formData.append('bakery_id', id)
        let res = await fetch('/bakery-comment', {
            method: "POST",
            body: formData,
        })
        let result = await res.json();
        checkLoginStatus(result);
        if (result.result == "comment posted") {
            form.querySelector('textarea').value = "";
            let commentRes = await fetch(`/bakery-comments/${id}`);
            let comments = await commentRes.json();
            console.log(comments);
            let commentDIV = document.querySelector('.each-info.comments');
            commentDIV.innerHTML = "";
            for (let comment of comments) {
                commentDIV.innerHTML += `
                <p><span><b>${comment.name}</b> ${comment.content}</span></p>
                `
            }
        }
    })
}


async function getBakery(id) {
    toPage('bakery')

    // load bakery infos
    let res = await fetch(`/get-bakery/${id}`);
    let bakery = await res.json();
    //console.log(bakery);

    // load bakery photos
    // let photoRes = await fetch(`/bakery-photos/${id}`);
    // let photos = await photoRes.json();
    // console.log(photos)

    // load bakery reviews
    let reviewRes = await fetch(`/bakery-reviews/${id}`);
    let reviews = await reviewRes.json();
    console.log(reviews);

    // load bakery comments
    let commentRes = await fetch(`/bakery-comments/${id}`);
    let comments = await commentRes.json();
    //console.log(comments);

    // load bakery like
    let likeRes = await fetch(`/like-bakery/${id}`);
    let like = await likeRes.json();
    if (like.result == true) {
        let likeBtn = document.querySelector('#like');
        likeBtn.classList.replace('far', 'fas');
    }

    let carousel = document.querySelector('.carousel-inner');
    let indicators = document.querySelector('.carousel-indicators');
    carousel.innerHTML = "";
    indicators.innerHTML = "";

    // load photos into carousel
    //  let imgDiv = document.querySelector('.carousel-inner > .carousel-item')
    carousel.innerHTML += `
        <div class="carousel-item active bakery-hero-img-wrapper">
         <img class="bakery-hero-img" src="./bakery_photos/high-class/${bakery[0].cover_photos}" class="d-block w-100" alt="...">
        </div>
        `

    // load main info of bakery
    let nameDIV = document.createElement('div')
    nameDIV.classList = 'each-info each-name';
    nameDIV.setAttribute("data-id", bakery[0].id);
    nameDIV.innerHTML = `        <div class="back">
    <i class="fas fa-arrow-left" onclick="toPage('bakery-list')"></i>
</div>` + `<div class="each-bakery-name">${bakery[0].name}</div>` + `<div>Information</div>`

    let contentDIV = document.createElement('div')
    contentDIV.classList = 'each-info each-content';
    contentDIV.innerHTML = `
        <span class="each-bakery-info">
            <span>Address: </span>
            <span>${bakery[0].address}</span>
        </span>
        <span class="each-bakery-info">
            <span>Phone: </span>
            <span>${bakery[0].phone}</span>
        </span>
        <span class="each-bakery-info">
            <span>Opening hours: </span>
            <span>${bakery[0].opening_hours}</span>
        </span>
        `

    // load reviews
    let reviewDIV = document.createElement('div')
    reviewDIV.classList = 'each-info reviews';
    let newReviewBtn = document.createElement('button')
    newReviewBtn.classList.add('new-review');
    newReviewBtn.setAttribute('onclick', `toReview(${bakery[0].id})`)
    newReviewBtn.innerHTML = `Share your review`
    reviewDIV.innerHTML += `<h5 style="text-align: center; font-weight: 500">Read others' insightful thoughts</h5>`
    for (let review of reviews) {
        // console.log(review)
        console.log(review.id)
        reviewDIV.innerHTML += `
        <p onclick="redirectToReview(${review.id})">
        <b>${review.title}</b>
        <br>
        ${review.name}
        </p>
        `
    }
    reviewDIV.appendChild(newReviewBtn)


    // load comments
    let commentDIV = document.createElement('div')
    commentDIV.classList = 'each-info comments';
    commentDIV.innerHTML += `<h5 style="text-align: center; font-weight: 500">Leave a short comment here</h5>`
    for (let comment of comments) {
        commentDIV.innerHTML += `
        <p style="margin-bottom: 0px"><span><b>${comment.name}: </b> ${comment.content}</span></p>
        `
    }
    commentDIV.innerHTML += `
    <form action="/bakery-comment" id="bakery-comment">
        <textarea id="comment"  name="content" required></textarea>
        <input id="comment-submit" type="submit" value="Submit">
    </form>
    `

    let reviewCommentContainer = document.createElement("div")
    reviewCommentContainer.classList = 'bakery-review-comment-container'
    reviewCommentContainer.appendChild(reviewDIV)
    reviewCommentContainer.appendChild(commentDIV)


    for (let el of [nameDIV, contentDIV, reviewCommentContainer]) {
        document.querySelector('#bakery-context').appendChild(el)
    }
    // document.querySelector('#bakery-context').innerHTML += `
    // <form action="/bakery-comment" id="bakery-comment">
    //     <textarea class="each-info comment" name="content" required></textarea>
    //     <input type="submit" value="post">
    // </form>
    // `


    selectCommentForm();
    selectBakeryLikeButton(bakery[0].id);
}

// create and redirect to create-review page
async function toReview(bakeryID) {
    await toPage('new-review');

    let res = await fetch(`get-bakery/${bakeryID}`)
    let bakery = await res.json();
    document.querySelector('#new-review > label').innerHTML = `Reviewing ${bakery[0].name}`;
    document.querySelector('#new-review').setAttribute("data-id", bakeryID);
    selectReviewForm()

}

async function selectReviewForm() {
    document.querySelector('#new-review').addEventListener('submit', async event => {
        event.preventDefault();
        let id = document.querySelector('#new-review').getAttribute("data-id");

        // let form = event.target
        // let formData = new FormData()
        // formData.append('bakery_id', id)
        // formData.append("title", form.title.value)
        // formData.append("content", form.content.value)

        let form = document.querySelector('#new-review');
        let formData = new FormData(form);
        formData.append('bakery_id', id)
        // formData.append("uploads", form.uploads.files[0])
        let res = await fetch('/review', {
            method: "POST",
            body: formData,
        })
        let result = await res.json()
        console.log(result)
        if (result.result == "good") {
            toPage("reviews")
        } 

        checkLoginStatus(result);
    })
}

async function selectBakeryLikeButton(bakeryID) {
    let likeBtn = document.querySelector('#like')
    // console.log(likeBtn);
    // console.log(bakeryID);

    likeBtn.addEventListener('click', async (event) => {
        let res = await fetch(`/like-bakery/${bakeryID}`, { method: "POST" })
        let like = await res.json()
        checkLoginStatus(like);
        if (like.result == true) {
            console.log("TRUE");
            likeBtn.classList.replace("far", "fas");
        } else if (like.result == false) {
            console.log("FALSE")
            likeBtn.classList.replace("fas", "far");
        }
    })
}