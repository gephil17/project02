var Scraper = require('images-scraper');
const puppeteer = require('puppeteer')

const google = new Scraper({
    puppeteer: {
        headless: false,
    }
});

(async () => {
    // library
    const results = await google.scrape('丹麥條 麵包', 5);
    console.log(results)
    // 菠蘿包 腸仔包 雞尾包 丹麥條 蛋撻 椰撻
    puppeteer
    const browser = await puppeteer.launch({
        headless: false
    });

    let page = await browser.newPage();
    for (let result of results) {
        try {
            await page.goto(result.url)
            await page.$eval("body", async () => {
                // new div
                const ahrefDiv = document.createElement('a')
                ahrefDiv.setAttribute('href', (document.querySelector('body > img')).getAttribute('src'))
                ahrefDiv.setAttribute('download', "")

                // img div
                const parentNode = document.querySelector('body > img').parentNode
                const imgElement = document.querySelector('body > img')
                parentNode.insertBefore(ahrefDiv, imgElement)


                await document.querySelector('body > a').click()

            })
        } finally {
            continue
        }

    }
})();
