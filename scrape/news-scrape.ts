import puppeteer from 'puppeteer';
import json from 'jsonfile'

// puppeteer method
async function scraping() {
    const browser = await puppeteer.launch({
        headless: false
    });
    let page = await browser.newPage();
    async function scrapePage() {
        async function readWrite(result: any) {
            let readJson = await json.readFile("news.json", "utf8")

            result.forEach((element: any) => {
                readJson.push(element)
            })

            await json.writeFile("news.json", readJson, { spaces: 2 })
            return
        }
        let searchResult = await page.$$eval(".g", (resultsList: any) => {
            let result: Object[] = []
            for (let results of resultsList) {
                let info = {}
                info = {
                    link: results.querySelector('.rc > .yuRUbf > a')?.getAttribute("href"),
                    title: results.querySelector('.rc > .yuRUbf > a > h3 > span')?.innerHTML,
                    content: results.querySelector('.rc > .IsZvec > div > .aCOpRe > span:nth-child(2)')?.innerHTML
                }
                result.push(info)
            }
            return result
            // console.log(result)
        })
        await readWrite(searchResult)
    }
    await page.goto("https://www.google.com/search?biw=1188&bih=1030&tbs=qdr%3Ay&ei=5LXhX_POEKTFmAXbwYGQCw&q=%E9%BA%B5%E5%8C%85%E5%BA%97+-org+-job+-jooble+-dcard+-sznews+-travian+-sino+-appledaily+-mingpao+-openrice+-tripadvisor+-wikipedia+-youtube+-facebook+-instagram+-gov+-icac&oq=%E9%BA%B5%E5%8C%85%E5%BA%97+-org+-job+-jooble+-dcard+-sznews+-travian+-sino+-appledaily+-mingpao+-openrice+-tripadvisor+-wikipedia+-youtube+-facebook+-instagram+-gov+-icac&gs_lcp=CgZwc3ktYWIQA1AAWABgLmgAcAB4AIABAIgBAJIBAJgBAKoBB2d3cy13aXo&sclient=psy-ab&ved=0ahUKEwjz0MmNneHtAhWkIqYKHdtgALI4ChDh1QMIDQ&uact=5", { timeout: 0 })
    await scrapePage()
    await page.close()
}

scraping()
