import express from 'express';
import expressSession from 'express-session';
import path from 'path';
import dotenv from 'dotenv';
import { UsersService } from './services/UsersService';
import { BakeryService } from './services/BakeryService';
import { PicService } from './services/PicService';
import { ProfileService } from './services/ProfileService';

dotenv.config();

import Knex from 'knex';
import { UsersController } from './controllers/UsersController';
import { BakeryController} from './controllers/BakeryController';
import { PicController } from './controllers/PicController'
import { ProfileController } from './controllers/ProfileController';
import { isLoggedIn } from './guards';
// import grant from 'grant';
// import { LoadNewsController } from './controllers/LoadNewsController'
// import { MapController } from './controllers/MapController'
// import { MapService } from './services/MapService'

// const grantExpress = grant.express({
//     "defaults":{
//         "origin": "http://localhost:8100",
//         "transport": "session",
//         "state": true,
//     },
//     "google":{
//         "key": process.env.GOOGLE_CLIENT_ID || "",
//         "secret": process.env.GOOGLE_CLIENT_SECRET || "",
//         "scope": ["profile","email"],
//         "callback": "/login/google"
//     }
// });

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);
// const knex = Knex(knexConfig[process.env.NODE_ENV || "testing"]);

const app = express();
// app.use(grantExpress as express.RequestHandler);
app.use(
    expressSession({
        secret: 'Bread Runs Fast',
        resave: true,
        saveUninitialized: true,
    }),
);

const usersService = new UsersService(knex);
const bakeryService = new BakeryService(knex);
const picService = new PicService(knex);
const profileService = new ProfileService(knex);
export const usersController = new UsersController(usersService);
export const bakeryController = new BakeryController(bakeryService);
export const picController = new PicController(picService);
export const profileController = new ProfileController(profileService);

import {routes} from './routes';

app.use('/', routes);
app.get('/login-status', usersController.checkLogin)


// // load news
// const loadNewsController = new LoadNewsController
// app.get('/newsfeeds', loadNewsController.loadNews)

// // load news
// const mapService = new MapService(knex)
// const mapController = new MapController(mapService)
// app.get('/bakery-on-map', mapController.bakery)


function getPublicPage(page: string) {
    console.log('trigger get public XXX')
    app.get(`/${page}`, (req, res) => {
        console.log('inside trigger get public XXX')
        res.sendFile(path.resolve(`public/${page}.html`));
    })
}
getPublicPage('home');
getPublicPage('bakery-list');
getPublicPage('map');
getPublicPage('bakery')
getPublicPage('reviews');
getPublicPage('review');
getPublicPage('news');
getPublicPage('login');
getPublicPage('new-review');
getPublicPage('bread-types');


/* Dragon's comment: this is a very specific writing for your pure html single-page application */
/* */
// app.get(`/:targetPage`, (req, res, next) => {
//     let targetPage = req.params.targetPage;
//     console.log(targetPage)
//     res.sendFile(path.resolve(`public/${targetPage}.html`));
//     next();
// })


app.get(`/profile`, isLoggedIn, (req, res) => {
    res.sendFile(path.resolve(`protected/profile.html`));
})

app.use(express.static('public'));
app.use(express.static('photos'));
app.use(express.static('uploads'));
app.use(isLoggedIn, express.static('protected'));

app.use((req, res, next) => {
    if (req.method !== 'GET') {
        next;
        return;
    }
    res.send("404 Not Found.")
})


const PORT = 8100;
app.listen(PORT, () => {
    console.log('listening on http://127.0.0.1:' + PORT);
})
